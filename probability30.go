package main

import (
	"fmt"
)

func inc(a []int) bool {
	a[0] = a[0] + 1
	for i := range a {
		if a[i] < 2 {
			return true
		}
		a[i] = 0
		if i == len(a)-1 {
			return false
		}
		a[i+1] = a[i+1] + 1
	}
	return false
}

func goodAll(a []int) bool {
	for i := range a {
		if a[i] != a[0] {
			return false
		}
	}
	return true
}

const LEN = 4

func good(a []int) bool {
	for i := range a[:len(a)-LEN+1] {
		if goodAll(a[i : i+LEN]) {
			return true
		}
	}
	return false
}

func main() {
	res := 0
	a := make([]int, 30)
	fmt.Println("Hello")
	for i := 0; true; i++ {
		if good(a) {
			res++
		}
		if !inc(a) {
			break
		}
		if i%1000000 == 0 {
			fmt.Printf("i = %d; res = %d\na = %v\n", i, res, a)
		}
	}
	fmt.Printf("res = %d\n", res)
}
